# Interview Task (PILZ)

## Task
Docker is a technology that is often used within the DevOps area, and Docker Compose is a useful tool to allow the running on many different docker containers locally as a single solution.

Please create a docker compose configuration which will run 'AT LEAST' the following containers:

- Prometheus
- Alertmanager
- NGINX

A frontend should be available for each of these services on the localhost.

Configure the above system in such a way so as when the NGINX container is down, this is registered in the Prometheus system, which will then send an alert to Alertmanager. This alert should be displayed on the Alertmanager UI. You do not need to configure any routes from the Alertmanager to any other systems such as email or slack.

You may configure the system in any way you like, you can use additional containers if required.

## Resources
- Prometheus - (https://prometheus.io/)
- Alertmanager - (https://prometheus.io/docs/alerting/latest/alertmanager/)
- Docker - (https://www.docker.com/)
- Docker Compose - (https://docs.docker.com/compose/)

## Purpose
Alerting and Monitoring are an important part of the SRE role. While there are many different alerting systems, knowledge gained in one will be applicable to another. This test should show that you can build a local test monitoring and alerting configuration which could be used as a testbed for production systems. Docker is a ubiquitous tool, the ability to use and configure docker is essential.
